#!/bin/bash

# Enable exit on error
#set -e
#set -x

echo "Calculating files that were changed by the MR."

SOURCE_REPO="origin"
if [ -z "$CI_PROJECT_PATH" ]; then
    # Source is Local
    echo "Current branch: $(git branch --show-current)"
    SOURCE_BRANCH_PREFIX=""
    SOURCE_BRANCH="$(git branch --show-current)"
    TARGET_REPO="origin"
    TARGET_BRANCH="main"
else
    # Source is Remote
    echo "CI_PROJECT_PATH: $CI_PROJECT_PATH"
    SOURCE_BRANCH_PREFIX="$SOURCE_REPO/"
    if [[ "$CI_PROJECT_PATH" =~ ^"sosy-lab" ]]; then
        if [ -z "$CI_MERGE_REQUEST_SOURCE_PROJECT_PATH" ]; then
            echo "This CI is not for a merge-request."
            SOURCE_BRANCH_PREFIX=""
            SOURCE_BRANCH="HEAD"
            TARGET_REPO="origin"
            TARGET_BRANCH="main"
        else
            echo "CI_MERGE_REQUEST_SOURCE_PROJECT_PATH: $CI_MERGE_REQUEST_SOURCE_PROJECT_PATH"
            SOURCE_BRANCH="$CI_MERGE_REQUEST_SOURCE_BRANCH_NAME"
            TARGET_BRANCH="$CI_MERGE_REQUEST_TARGET_BRANCH_NAME"
            TARGET_REPO="origin"
            if [[ "$CI_MERGE_REQUEST_SOURCE_PROJECT_PATH" =~ ^"sosy-lab" ]]; then
                # Source is SoSy-Lab, i.e., for branches inside the SoSy-Lab repo
                echo "SoSy-Lab branch"
            else
                # Source is a fork, i.e., by GitLab users
                # Set up source repo (if not yet done)
                SOURCE_REPO=${CI_MERGE_REQUEST_SOURCE_PROJECT_PATH%\/*}
                SOURCE_BRANCH_PREFIX="$SOURCE_REPO/"
                git remote | grep "$SOURCE_REPO" || git remote add "$SOURCE_REPO" "https://gitlab.com/$CI_MERGE_REQUEST_SOURCE_PROJECT_PATH.git"
                git remote get-url "$SOURCE_REPO"
            fi
        fi
    else
        # Source is something like ultimate-pa
        # Set up upstream repo (if not yet done)
        git remote | grep upstream || git remote add upstream https://gitlab.com/sosy-lab/benchmarking/fm-tools.git
        git remote get-url upstream
        TARGET_REPO="upstream"
        SOURCE_BRANCH="main"
        TARGET_BRANCH="main"
    fi
fi
TARGET_BRANCH_PREFIX="$TARGET_REPO/"

git fetch --depth=200 "$SOURCE_REPO" "$SOURCE_BRANCH"
git fetch --depth=200 "$TARGET_REPO" "$TARGET_BRANCH"

MERGE_BASE_COMMIT=$(git merge-base "${SOURCE_BRANCH_PREFIX}${SOURCE_BRANCH}" "${TARGET_BRANCH_PREFIX}${TARGET_BRANCH}")
echo "Merge-base commit:"
echo "$MERGE_BASE_COMMIT"
CHANGED_YML_FILES=$(git diff --name-only --diff-filter=d "${MERGE_BASE_COMMIT}..${SOURCE_BRANCH_PREFIX}${SOURCE_BRANCH}" -- data/*.yml)
echo "Changed YML files:"
echo "$CHANGED_YML_FILES"
